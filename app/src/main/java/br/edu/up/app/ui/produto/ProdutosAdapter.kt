package br.edu.up.app.ui.produto

import android.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import br.edu.up.app.R
import br.edu.up.app.data.Fotos
import br.edu.up.app.data.Produto
import br.edu.up.app.databinding.FragmentItemProdutoBinding
import coil.load
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.text.DecimalFormat
import java.util.Locale

class ProdutosAdapter(
    private val produtos: List<Produto>,
    val viewModel: ProdutoViewModel
) : RecyclerView.Adapter<ProdutosAdapter.ProdutoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ProdutoViewHolder {

        return ProdutoViewHolder(
            FragmentItemProdutoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ProdutoViewHolder, position: Int) {
        val produto = produtos[position]

        holder.imgFoto.load(R.drawable.semfoto)

        Firebase.storage.getReference(produto.foto).downloadUrl
            .addOnSuccessListener { imageUrl ->
            holder.imgFoto.load(imageUrl)
        }


        //val imgId = Fotos.get(produto.foto)
        //holder.imgFoto.setImageResource(imgId)


        holder.txtNome.text = produto.nome
        val localBrasil = Locale("pt", "BR")
        val df = DecimalFormat.getCurrencyInstance(localBrasil)
        holder.txtPreco.text = "${df.format(produto.preco)}"

        holder.itemView.setOnClickListener { view ->
            viewModel.editar(produto)
            val action = ProdutosFragmentDirections.actionListaToProduto()
            view.findNavController().navigate(action)
        }

        holder.itemView.setOnLongClickListener { view ->
            AlertDialog.Builder(view.context)
                .setMessage("ATENÇÃO")
                .setPositiveButton("Confirmar") { dialog, id ->
                    viewModel.excluir(produto)
                }
                .setNegativeButton("CANCELAR") { dialog, id ->
                    //ignorar
                }.create().show()
            true
        }

    }

    override fun getItemCount(): Int = produtos.size

    inner class ProdutoViewHolder(binding: FragmentItemProdutoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val imgFoto: ImageView = binding.imgFoto
        val txtNome: TextView = binding.txtNome
        val txtPreco: TextView = binding.txtPreco
    }

}